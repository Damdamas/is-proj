﻿using System;
using System.Collections.Generic;

namespace ISproj.Models
{
    public partial class Equipment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Warehouseid { get; set; }
    }
}
