﻿using System;
using System.Collections.Generic;

namespace ISproj.Models
{
    public partial class Workschedule
    {
        public int Id { get; set; }
        public int Workerid { get; set; }
        public string Surname { get; set; }
        public string Date { get; set; }
        public int Starthour { get; set; }
        public int Endhour { get; set; }
        public int Hoursworked { get; set; }
    }
}
