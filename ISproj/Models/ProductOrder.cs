﻿using System;
using System.Collections.Generic;

namespace ISproj.Models
{
    public partial class ProductOrder
    {
        public uint Id { get; set; }
        public uint Amount { get; set; }
        public uint ReturnedAmount { get; set; }
        public bool Done { get; set; }
        public DateTime Date { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public uint? FkProduct { get; set; }
        public uint? FkClient { get; set; }

        public virtual Client FkClientNavigation { get; set; }
        public virtual Product FkProductNavigation { get; set; }
    }
}
