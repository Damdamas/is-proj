﻿using System;
using System.Collections.Generic;

namespace ISproj.Models
{
    public partial class Warehouse
    {
        public Warehouse()
        {
            ProductStock = new HashSet<ProductStock>();
        }

        public uint Id { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ProductStock> ProductStock { get; set; }
    }
}
