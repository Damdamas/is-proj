﻿using System;
using System.Collections.Generic;

namespace ISproj.Models
{
    public partial class Equipmentcheck
    {
        public DateTime Date { get; set; }
        public string Reason { get; set; }
        public string Location { get; set; }
        public string State { get; set; }
        public int Equipmentid { get; set; }
        public int Id { get; set; }
    }
}
