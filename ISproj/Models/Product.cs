﻿using System;
using System.Collections.Generic;

namespace ISproj.Models
{
    public partial class Product
    {
        public Product()
        {
            ProductOrder = new HashSet<ProductOrder>();
            ProductStock = new HashSet<ProductStock>();
        }

        public uint Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string Manufacturer { get; set; }
        public uint? Category { get; set; }

        public virtual ProductCategory CategoryNavigation { get; set; }
        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
        public virtual ICollection<ProductStock> ProductStock { get; set; }
    }
}
