﻿using System;
using System.Collections.Generic;

namespace ISproj.Models
{
    public partial class ProductCategory
    {
        public ProductCategory()
        {
            InverseParentNavigation = new HashSet<ProductCategory>();
            Product = new HashSet<Product>();
        }

        public uint Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public uint? Parent { get; set; }

        public virtual ProductCategory ParentNavigation { get; set; }
        public virtual ICollection<ProductCategory> InverseParentNavigation { get; set; }
        public virtual ICollection<Product> Product { get; set; }
    }
}
