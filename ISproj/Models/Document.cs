﻿using System;
using System.Collections.Generic;

namespace ISproj.Models
{
    public partial class Document
    {
        public string Name { get; set; }
        public DateTime Dateofcreation { get; set; }
        public string Author { get; set; }
        public string Text { get; set; }
        public int Equipmentid { get; set; }
        public int Id { get; set; }
    }
}
