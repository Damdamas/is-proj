﻿using System;
using System.Collections.Generic;

namespace ISproj.Models
{
    public partial class Note
    {
        public string Text { get; set; }
        public DateTime Date { get; set; }
        public int Equipmentid { get; set; }
        public int Id { get; set; }
    }
}
