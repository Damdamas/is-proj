﻿using System;
using System.Collections.Generic;

namespace ISproj.Models
{
    public partial class Client
    {
        public Client()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }

        public uint Id { get; set; }
        public string Mail { get; set; }
        public int Role { get; set; }
        public byte[] Hash { get; set; }
        public byte[] Salt { get; set; }

        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
