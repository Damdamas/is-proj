﻿using System;
using System.Collections.Generic;

namespace ISproj.Models
{
    public partial class Worker
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Date { get; set; }
        public int Thworked { get; set; }
        public int Clientid { get; set; }
    }
}
