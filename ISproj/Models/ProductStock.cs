﻿using System;
using System.Collections.Generic;

namespace ISproj.Models
{
    public partial class ProductStock
    {
        public uint Id { get; set; }
        public uint Amount { get; set; }
        public uint? Product { get; set; }
        public uint? Location { get; set; }

        public virtual Warehouse LocationNavigation { get; set; }
        public virtual Product ProductNavigation { get; set; }
    }
}
