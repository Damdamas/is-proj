﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Data;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Reflection;
using Microsoft.AspNetCore.Http;
using Microsoft.JSInterop;

namespace ISproj
{
	//magija
	public static class ProductExport
	{
		public static byte[] CreateExcelFile(IEnumerable<Models.Product> prods, DbSet<Models.ProductCategory> cats, string filename)
		{
			var stream = new MemoryStream();
			using (var document = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook))
			{
				var workbookPart = document.AddWorkbookPart();
				workbookPart.Workbook = new Workbook();
				var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
				worksheetPart.Worksheet = new Worksheet();
				var workbookStylesPart = workbookPart.AddNewPart<WorkbookStylesPart>();
				GenerateWorkbookStylesPartContent(workbookStylesPart);
				var sheets = workbookPart.Workbook.AppendChild(new Sheets());
				var sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };
				sheets.Append(sheet);
				workbookPart.Workbook.Save();
				var sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());

				var header = new Row();
				string[] headers = { "Id", "Name", "Description", "Price", "Manufacturer", "Category" };
				foreach (string s in headers)
				{
					header.Append(new Cell()
					{
						CellValue = new CellValue(s),
						DataType = new EnumValue<CellValues>(CellValues.String)
					});
				}
				sheetData.AppendChild(header);


				foreach (Models.Product prod in prods)
				{
					var row = new Row();

					row.Append(new Cell()
					{
						CellValue = new CellValue(prod.Id.ToString()),
						DataType = new EnumValue<CellValues>(CellValues.String)
					});
					row.Append(new Cell()
					{
						CellValue = new CellValue(prod.Name.ToString()),
						DataType = new EnumValue<CellValues>(CellValues.String)
					});
					row.Append(new Cell()
					{
						CellValue = new CellValue(prod.Description.ToString()),
						DataType = new EnumValue<CellValues>(CellValues.String)
					});
					row.Append(new Cell()
					{
						CellValue = new CellValue(prod.Price.ToString()),
						DataType = new EnumValue<CellValues>(CellValues.String)
					});
					row.Append(new Cell()
					{
						CellValue = new CellValue(prod.Manufacturer.ToString()),
						DataType = new EnumValue<CellValues>(CellValues.String)
					});
					row.Append(new Cell()
					{
						CellValue = new CellValue( cats.Find(prod.Category).Name ),
						DataType = new EnumValue<CellValues>(CellValues.String)
					});

					sheetData.AppendChild(row);
				}
				workbookPart.Workbook.Save();
			}
			stream.Close();

			//var result = new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			//result.FileDownloadName = filename;

			return stream.ToArray();
		}

		#region utils

		public static object GetValue(object target, string name)
		{
			return target.GetType().GetProperty(name).GetValue(target);
		}

		public static IEnumerable<KeyValuePair<string, Type>> GetProperties(Type type)
		{
			return type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
					.Where(p => p.CanRead && IsSimpleType(p.PropertyType)).Select(p => new KeyValuePair<string, Type>(p.Name, p.PropertyType));
		}

		public static bool IsSimpleType(Type type)
		{
			var underlyingType = type.IsGenericType &&
				type.GetGenericTypeDefinition() == typeof(Nullable<>) ?
				Nullable.GetUnderlyingType(type) : type;

			var typeCode = Type.GetTypeCode(underlyingType);

			switch (typeCode)
			{
				case TypeCode.Boolean:
				case TypeCode.Byte:
				case TypeCode.Char:
				case TypeCode.DateTime:
				case TypeCode.Decimal:
				case TypeCode.Double:
				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
				case TypeCode.SByte:
				case TypeCode.Single:
				case TypeCode.String:
				case TypeCode.UInt16:
				case TypeCode.UInt32:
				case TypeCode.UInt64:
					return true;
				default:
					return false;
			}
		}

		private static bool IsNumeric(TypeCode typeCode)
		{
			switch (typeCode)
			{
				case TypeCode.Decimal:
				case TypeCode.Double:
				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
				case TypeCode.UInt16:
				case TypeCode.UInt32:
				case TypeCode.UInt64:
					return true;
				default:
					return false;
			}
		}

		private static void GenerateWorkbookStylesPartContent(WorkbookStylesPart workbookStylesPart1)
		{
			Stylesheet stylesheet1 = new Stylesheet() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "x14ac x16r2 xr" } };
			stylesheet1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
			stylesheet1.AddNamespaceDeclaration("x14ac", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac");
			stylesheet1.AddNamespaceDeclaration("x16r2", "http://schemas.microsoft.com/office/spreadsheetml/2015/02/main");
			stylesheet1.AddNamespaceDeclaration("xr", "http://schemas.microsoft.com/office/spreadsheetml/2014/revision");

			Fonts fonts1 = new Fonts() { Count = (UInt32Value)1U, KnownFonts = true };

			Font font1 = new Font();
			FontSize fontSize1 = new FontSize() { Val = 11D };
			Color color1 = new Color() { Theme = (UInt32Value)1U };
			FontName fontName1 = new FontName() { Val = "Calibri" };
			FontFamilyNumbering fontFamilyNumbering1 = new FontFamilyNumbering() { Val = 2 };
			FontScheme fontScheme1 = new FontScheme() { Val = FontSchemeValues.Minor };

			font1.Append(fontSize1);
			font1.Append(color1);
			font1.Append(fontName1);
			font1.Append(fontFamilyNumbering1);
			font1.Append(fontScheme1);

			fonts1.Append(font1);

			Fills fills1 = new Fills() { Count = (UInt32Value)2U };

			Fill fill1 = new Fill();
			PatternFill patternFill1 = new PatternFill() { PatternType = PatternValues.None };

			fill1.Append(patternFill1);

			Fill fill2 = new Fill();
			PatternFill patternFill2 = new PatternFill() { PatternType = PatternValues.Gray125 };

			fill2.Append(patternFill2);

			fills1.Append(fill1);
			fills1.Append(fill2);

			Borders borders1 = new Borders() { Count = (UInt32Value)1U };

			Border border1 = new Border();
			LeftBorder leftBorder1 = new LeftBorder();
			RightBorder rightBorder1 = new RightBorder();
			TopBorder topBorder1 = new TopBorder();
			BottomBorder bottomBorder1 = new BottomBorder();
			DiagonalBorder diagonalBorder1 = new DiagonalBorder();

			border1.Append(leftBorder1);
			border1.Append(rightBorder1);
			border1.Append(topBorder1);
			border1.Append(bottomBorder1);
			border1.Append(diagonalBorder1);

			borders1.Append(border1);

			CellStyleFormats cellStyleFormats1 = new CellStyleFormats() { Count = (UInt32Value)1U };
			CellFormat cellFormat1 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U };

			cellStyleFormats1.Append(cellFormat1);

			CellFormats cellFormats1 = new CellFormats() { Count = (UInt32Value)2U };
			CellFormat cellFormat2 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U };
			CellFormat cellFormat3 = new CellFormat() { NumberFormatId = (UInt32Value)14U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyNumberFormat = true };

			cellFormats1.Append(cellFormat2);
			cellFormats1.Append(cellFormat3);

			CellStyles cellStyles1 = new CellStyles() { Count = (UInt32Value)1U };
			CellStyle cellStyle1 = new CellStyle() { Name = "Normal", FormatId = (UInt32Value)0U, BuiltinId = (UInt32Value)0U };

			cellStyles1.Append(cellStyle1);
			DifferentialFormats differentialFormats1 = new DifferentialFormats() { Count = (UInt32Value)0U };
			TableStyles tableStyles1 = new TableStyles() { Count = (UInt32Value)0U, DefaultTableStyle = "TableStyleMedium2", DefaultPivotStyle = "PivotStyleLight16" };

			StylesheetExtensionList stylesheetExtensionList1 = new StylesheetExtensionList();

			StylesheetExtension stylesheetExtension1 = new StylesheetExtension() { Uri = "{EB79DEF2-80B8-43e5-95BD-54CBDDF9020C}" };
			stylesheetExtension1.AddNamespaceDeclaration("x14", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main");

			StylesheetExtension stylesheetExtension2 = new StylesheetExtension() { Uri = "{9260A510-F301-46a8-8635-F512D64BE5F5}" };
			stylesheetExtension2.AddNamespaceDeclaration("x15", "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main");

			OpenXmlUnknownElement openXmlUnknownElement4 = OpenXmlUnknownElement.CreateOpenXmlUnknownElement("<x15:timelineStyles defaultTimelineStyle=\"TimeSlicerStyleLight1\" xmlns:x15=\"http://schemas.microsoft.com/office/spreadsheetml/2010/11/main\" />");

			stylesheetExtension2.Append(openXmlUnknownElement4);

			stylesheetExtensionList1.Append(stylesheetExtension1);
			stylesheetExtensionList1.Append(stylesheetExtension2);

			stylesheet1.Append(fonts1);
			stylesheet1.Append(fills1);
			stylesheet1.Append(borders1);
			stylesheet1.Append(cellStyleFormats1);
			stylesheet1.Append(cellFormats1);
			stylesheet1.Append(cellStyles1);
			stylesheet1.Append(differentialFormats1);
			stylesheet1.Append(tableStyles1);
			stylesheet1.Append(stylesheetExtensionList1);

			workbookStylesPart1.Stylesheet = stylesheet1;
		}

		#endregion

	}
}
