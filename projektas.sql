-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2019 m. Grd 11 d. 10:44
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `isproj`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `Client`
--

CREATE TABLE `Client` (
  `id` int(10) UNSIGNED NOT NULL,
  `mail` varchar(255) COLLATE utf8mb4_lithuanian_ci NOT NULL,
  `role` int(10) NOT NULL,
  `hash` binary(64) NOT NULL,
  `salt` binary(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `Client`
--

INSERT INTO `Client` (`id`, `mail`, `role`, `hash`, `salt`) VALUES
(1, 'linas@admin.ktu.lt', 0, 0xaf0f2cc944247f5d533822a983be950669da7c964bece1baa4bdcaea5fe46b2b93e10584291f1ab23973e4dd2ea21c3caf520625cf4f590de6abb5cc2d5af28e, 0x19269078b18528e30af70742f9bba55774c9922f9ffcc0025f209fb2b124e2663d91a85343b93ad14ac9db3682928f8c0260f5c6495ee817af6a0f92b1315e8a2ae1de5e6baf85705a7fd8d5b664d291ae03dcd86adc77a2eee1e155df08c1e49d8540a7bc3065a33bd2b4f0afc6854d89bc7112128eb736bfdf8840dcfdf427),
(2, 'linas@gmail.com', 1, 0xb3706f7f2fea6ee50116e18aeda6c085a40f9f9e23eb9367ee457618f1cab40626ded816ccc23c30e7cd0a064148e8c407a7e4fefae8f33f802c8c8eb428fb9f, 0x518bb0b3fb0d600392dae8f7d28be1f5b15593a63286fd852edad0b610299643e5bfeb757e35db6ead92bab00e0392804fc17ffb2ec0719b6f2a689a1fbd73174351dafbd3e53b0e9da5d96618ca63ba3c86cc67b2971991ad5fd81bec51a347aff5236f77284fcf5eeee4205bd02296ca8feb39b8fba30e43cc01a326c937d0),
(3, 'bandom@gmail.com', 1, 0x089a77e695755d0b20ecdf1beb3e74751450e165382556152ce6d0752288f98bf82a5f50d0f3e294232c5997379caaea069d235933edea938acd0695f05a681c, 0x55bfbfae9902691ec532d33aefeebe9b8dd4ca63927639eee4277b22531a9171b93648e1891602f68122cd5938548b288feda3fda147ecd01c25043f6b82d52414e649e48b800c741b2d06318ed70cbe32b4b1ad92653be1bdc70b6e116af23f8b9d0c16a07041fca479eff588ac5246448cfc51680dcf88b543122bbb5433f4),
(4, 'bandom@gmail1.com', 0, 0x3a8f9973d6954d9d47f6aadd743557a3fee344979efdda29336b0511c56c33dccf136c026c62de5c578e353557b1e478da52d9dc9cf5aea9c9fc6e55341d9e61, 0xbbbda54393148e9af89d31e4749b930a835cb47b96d25c43c583046ff9c3f3342b12384ea01fc8a3f9918d71e65f2de1230d17ee285cc613ce863760a66f34e450a2ae51a54f233095a1a15dcb7fac4160a883d9ef7d1f2c9ad771de4ae1b45a6101193373833dfc40ebf634d2a7cf3624aa4d32c62e91bedf83426495c5f856),
(5, 'ugnius227@gmail.com', 1, 0xf1c09c3b5870262b05186b29c23d50afdd3e94ef7bbf2a9d18a4f7687c49bca051eb73689aa843ae4ec50bf9d8d0d24381f75456f6057954ba13f02109c0eae6, 0x3336218bef21db9dc65d5ebdfe50a9b46f30a2535f6166a1577da93c69c16647cbd1a9370fd2c189ec9b7e7698510c2070d63c31f066699b1cafab2639506cd97261412b31a9c245e04ee431036a3432feb87825a3dfc6ea561cd217bbcf52c0b0caa9a8e1f8d0286809f34f1ac0e8696157b1ca4dc89e84d7be4475a59b90a6),
(6, 'bandom@gmail21.com', 1, 0xaaa675a5163695becbaf8015578eabb9d348f49bc086eed0747f32ab55f1376c4dc06e08b67b668222e247dab0b427903511b6300ca5aef4267121f604aab8d8, 0x68d54935695274a2d8e2c0cad9672529105eac448f6c2116ad250fa9c537a20ad55e98e812e4e96b0534c43834cbf9b02313be2c35771cb7d4c773240fe968dedabcf4772954e488006688f17ec519a58a785c23d27d03589330e59fff17883aeafb4c34469516d5ddfe7290ed632a02c918a8eb6487f24431f51e0500cf871c),
(7, 'bandom@admin.ktu.lt', 0, 0xfe0d509500dd0d2619a0799a2f0441111b5426f6726887a98c13447937d496fe2d9b4fc2f6283f36074ca51af2ae065563cffd2a47ae3f7ef534e7576367443e, 0xabecb1f53672937dbd01187d0807de59125d04b9b51e1ce601dc69a19e6f57738bdb4019bca805133cdeac639f13dc7d9d5e3c7d985a6cc106a5cbd6d434749c8ca6782311d451e86848880a95eab49763725d83248adb9fa308023e4117677f995bc07c878ff69767355c3da421e2d201804d61575cf16f27504c9965adabc0);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `Document`
--

CREATE TABLE `Document` (
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dateofcreation` date NOT NULL,
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `text` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Equipmentid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `Equipment`
--

CREATE TABLE `Equipment` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Warehouseid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `Equipmentcheck`
--

CREATE TABLE `Equipmentcheck` (
  `Date` date NOT NULL,
  `reason` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `location` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Equipmentid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `Note`
--

CREATE TABLE `Note` (
  `text` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `Equipmentid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `Product`
--

CREATE TABLE `Product` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_lithuanian_ci NOT NULL,
  `description` text COLLATE utf8mb4_lithuanian_ci,
  `price` decimal(10,2) UNSIGNED NOT NULL,
  `manufacturer` varchar(255) COLLATE utf8mb4_lithuanian_ci DEFAULT NULL,
  `category` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `Product`
--

INSERT INTO `Product` (`id`, `name`, `description`, `price`, `manufacturer`, `category`) VALUES
(1, 'test', '--', '10', '--', NULL);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `ProductCategory`
--

CREATE TABLE `ProductCategory` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_lithuanian_ci NOT NULL,
  `description` text COLLATE utf8mb4_lithuanian_ci,
  `parent` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_lithuanian_ci;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `ProductOrder`
--

CREATE TABLE `ProductOrder` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` int(10) UNSIGNED NOT NULL,
  `returnedAmount` int(10) UNSIGNED NOT NULL,
  `done` tinyint(1) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deliveryDate` datetime DEFAULT NULL,
  `fkProduct` int(10) UNSIGNED DEFAULT NULL,
  `fkClient` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `ProductOrder`
--

INSERT INTO `ProductOrder` (`id`, `amount`, `returnedAmount`, `done`, `date`, `deliveryDate`, `fkProduct`, `fkClient`) VALUES
(1, 8, 0, 0, '2019-12-07 16:09:02', NULL, 1, 2),
(2, 3, 0, 0, '2019-12-07 16:16:11', NULL, 1, 2),
(3, 8, 0, 0, '2019-12-11 11:33:51', NULL, 1, 6);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `ProductStock`
--

CREATE TABLE `ProductStock` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` int(10) UNSIGNED NOT NULL,
  `Product` int(10) UNSIGNED DEFAULT NULL,
  `location` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_lithuanian_ci;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `Warehouse`
--

CREATE TABLE `Warehouse` (
  `id` int(10) UNSIGNED NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_lithuanian_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_lithuanian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_lithuanian_ci;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `Worker`
--

CREATE TABLE `Worker` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `date` varchar(20) NOT NULL,
  `thworked` int(11) NOT NULL,
  `Clientid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `Workschedule`
--

CREATE TABLE `Workschedule` (
  `id` int(11) NOT NULL,
  `Workerid` int(11) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `date` varchar(20) NOT NULL,
  `starthour` int(11) NOT NULL,
  `endhour` int(11) NOT NULL,
  `hoursworked` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Client`
--
ALTER TABLE `Client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Equipment`
--
ALTER TABLE `Equipment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Product`
--
ALTER TABLE `Product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `ProductCategory`
--
ALTER TABLE `ProductCategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `ProductOrder`
--
ALTER TABLE `ProductOrder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkProduct` (`fkProduct`),
  ADD KEY `fkClient` (`fkClient`);

--
-- Indexes for table `ProductStock`
--
ALTER TABLE `ProductStock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Product` (`Product`),
  ADD KEY `location` (`location`);

--
-- Indexes for table `Warehouse`
--
ALTER TABLE `Warehouse`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Worker`
--
ALTER TABLE `Worker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Workschedule`
--
ALTER TABLE `Workschedule`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Client`
--
ALTER TABLE `Client`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `Equipment`
--
ALTER TABLE `Equipment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Product`
--
ALTER TABLE `Product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ProductCategory`
--
ALTER TABLE `ProductCategory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ProductOrder`
--
ALTER TABLE `ProductOrder`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ProductStock`
--
ALTER TABLE `ProductStock`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Warehouse`
--
ALTER TABLE `Warehouse`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Worker`
--
ALTER TABLE `Worker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Workschedule`
--
ALTER TABLE `Workschedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Apribojimai eksportuotom lentelėm
--

--
-- Apribojimai lentelei `Product`
--
ALTER TABLE `Product`
  ADD CONSTRAINT `Product_ibfk_1` FOREIGN KEY (`category`) REFERENCES `ProductCategory` (`id`);

--
-- Apribojimai lentelei `ProductCategory`
--
ALTER TABLE `ProductCategory`
  ADD CONSTRAINT `ProductCategory_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `ProductCategory` (`id`);

--
-- Apribojimai lentelei `ProductOrder`
--
ALTER TABLE `ProductOrder`
  ADD CONSTRAINT `ProductOrder_ibfk_1` FOREIGN KEY (`fkProduct`) REFERENCES `Product` (`id`),
  ADD CONSTRAINT `ProductOrder_ibfk_2` FOREIGN KEY (`fkClient`) REFERENCES `Client` (`id`);

--
-- Apribojimai lentelei `ProductStock`
--
ALTER TABLE `ProductStock`
  ADD CONSTRAINT `ProductStock_ibfk_1` FOREIGN KEY (`Product`) REFERENCES `Product` (`id`),
  ADD CONSTRAINT `ProductStock_ibfk_2` FOREIGN KEY (`location`) REFERENCES `Warehouse` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
